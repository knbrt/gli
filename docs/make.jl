using Pkg
# cache current environment
activeproject = Base.active_project()

# activate doc environment
Pkg.activate(dirname(@__FILE__))

# start from parent dir
Pkg.add(PackageSpec(url=pwd()))

# for ci cd purposes
Pkg.instantiate()

# mage docs
using Documenter
using gli

makedocs(sitename="gli.jl", repo = "https://gitlab.com/knbrt/gli.jl")

# activate cached environment
Pkg.activate(activeproject)

