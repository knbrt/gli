using PackageCompiler

p = dirname(dirname(@__FILE__))

create_app(p, joinpath(p, "compiled"); precompile_execution_file=joinpath(p, "scripts", "precompile.jl"), force=true, filter_stdlibs=true)
