module gli

using JSON
using TOML
using Dates

import CSV
using Diana
using DataFrames


include("querries.jl")
include("parsers.jl")

function saveToHome(l::Vector{Any}, currentUser::String)
    df = DataFrame(l)
    unique!(df)
    something.(df, missing) |> CSV.write(joinpath(homedir(), "TimeTable_$(currentUser).csv"), bom=true)
end


function initClient(endpoint::String="origin")
    creds = TOML.parsefile(joinpath(homedir(), ".config", "gli", "credentials.toml"))
    # creating the GraphQLClient
    client = GraphQLClient(creds[endpoint]["baseURL"])

    client.serverAuth("Bearer $(creds[endpoint]["token"])")
    client.headers(Dict("header"=>"value"))
    return client
end


function query(q::String; valueOnly=false::Bool)
    client = initClient()
    query = querries[q]
    ans  = client.Query(query)
    valueOnly && return JSON.parse(ans.Data)
    return ans
end

function julia_main()::Cint
    try
        real_main()
    catch
        Base.invokelatest(Base.display_error, Base.catch_stack())
        return 1
    end
    return 0
end


function real_main()
    row = []
    currentUser = query("currentUser", valueOnly=true)["data"]["currentUser"]["username"]
    for k ∈ query("issuesGroups", valueOnly=true)["data"]["currentUser"]["groupMemberships"]["nodes"]
        group = k["group"]["fullPath"]
        @show group
        for kk ∈ k["group"]["projects"]["nodes"]
            project = kk["name"]
            projectKey = kk["fullPath"]
            @show project
            rows = parseIssues(kk["issues"], group, project, projectKey)
            push!(row, rows...)
        end
    end
    for k ∈ query("issuesProjects", valueOnly=true)["data"]["currentUser"]["projectMemberships"]["nodes"]
        if isnothing(k["project"]["group"])
            group = currentUser
        else
            group = k["project"]["group"]["fullPath"]
        end
        @show group
        project = k["project"]["name"]
        projectKey = k["project"]["fullPath"]
        @show project
        if "issues" ∈ keys(k["project"])
            rows = parseIssues(k["project"]["issues"], group, project, projectKey)
            push!(row, rows...)
        else
            push!(row, (group=group, project=project, projectKey=projectKey))
        end
    end
    saveToHome(row, currentUser)
end

if abspath(PROGRAM_FILE) == @__FILE__
    real_main()
end


end # module
