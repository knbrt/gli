function hmss(dt)
    (d, r) = divrem(dt, 8 * 60 * 60)
    (h, r) = divrem(r, 60 * 60)
    (m, s) = divrem(r, 60)
    d != 1 ? (ds = "s") : (ds = "")
    h != 1 ? (hs = "s") : (hs = "")
    m != 1 ? (ms = "s") : (ms = "")
    s != 1 ? (ss = "s") : (ss = "")
    ans = []
    times = [d, h, m, s]
    timenames = ["workday", "hour", "minute", "second"]
    plurals = [ds, hs, ms, ss]
    for i in 1:length(times)
        length(ans) > 0 && begin
            push!(ans, "$(times[i]) $(timenames[i])$(plurals[i])")
            continue
        end
        times[i] > 0 && begin
            push!(ans, "$(times[i]) $(timenames[i])$(plurals[i])")
            continue
        end
    end
    return join(ans, ", ")
end

function parseIssues(d::Dict{String, Any}, group, project, projectKey)
    if length(d["nodes"]) == 0
        ans = [(
            group = group,
            project = project,
            projectKey = projectKey,
            id = "",
            iid = "",
            webUrl = "",
            reference = "",
            issue = "",
            description = "",
            state = "",
            dueDate = "",
            confidential = "",
            milestone = "",
            milestoneWebPath = "",
            milestoneState = "",
            milestoneStartDate = "",
            milestoneDueDate = "",
            labels = "",
            timeEstimation = "",
            timeEstimationSeconds = "",
            totalTimeSpent = "",
            totalTimeSpentSeconds = "",
            assignees = "",
            userName = "",
            userEmail = "",
            timeSpent = "",
            timeSpentSeconds = "",
            timeSpentAt = "",
            note ="")]
        return ans
    else
        ans = []
        for i ∈ d["nodes"]
            # parse id
            id = i["id"]
            # parse project wide id
            iid = i["iid"]
            # parse webUrl
            webUrl = i["webUrl"]
            # parse reference
            reference = i["reference"]
            # parse title
            issue = i["title"]
            # parse description
            description = i["description"]
            # parse state
            state = i["state"]
            # parse dueDate
            dueDate = i["dueDate"]
            # parse confidential
            confidential = i["confidential"]
            # parse milestone
            if isnothing(i["milestone"])
                milestone = ""
                milestoneWebPath = ""
                milestoneState = ""
                milestoneStartDate = ""
                milestoneDueDate = ""
            else
                milestone = i["milestone"]["title"]
                milestoneWebPath = i["milestone"]["webPath"]
                milestoneState = i["milestone"]["state"]
                milestoneStartDate = i["milestone"]["startDate"]
                milestoneDueDate = i["milestone"]["dueDate"]
            end
            # parse labels
            labelList = []
            for l ∈ i["labels"]["edges"]
                push!(labelList, l["node"]["title"])
            end
            labels = join(labelList, ", ")
            # parse author
            authorName = i["author"]["name"]
            authorEmail = i["author"]["publicEmail"]
            # parse assignees
            assignees = []
            for a in i["assignees"]["edges"]
                push!(assignees, a["node"]["name"])
            end
            assignees = join(assignees, ", ")
            # parse timeEstimation
            timeEstimationSeconds = i["timeEstimate"]
            timeEstimation = hmss(i["timeEstimate"])
            # parse totalTimeSpent
            totalTimeSpentSeconds = i["totalTimeSpent"]
            totalTimeSpent = hmss(i["totalTimeSpent"])
            # init timelog values - to be updated if timelog exists
            timeSpent = ""
            timeSpentSeconds = 0
            timeSpentAt = ""
            userName = ""
            userEmail = ""
            note = ""
            for ii ∈ i["timelogs"]["edges"]
                # parse timeSpent
                timeSpentSeconds = ii["node"]["timeSpent"]
                timeSpent = hmss(Int(ii["node"]["timeSpent"]))
                # parse timeSpentAt
                timeSpentAt = ii["node"]["spentAt"]
                # parse user
                userName = ii["node"]["user"]["name"]
                userEmail = ii["node"]["user"]["publicEmail"]
                # parse note
                if isnothing(ii["node"]["note"])
                    note = ""
                else
                    note = ii["node"]["note"]["body"]
                end
                # build the whole row
                irow = (
                    group = group,
                    project = project,
                    projectKey = projectKey,
                    id = id,
                    iid = iid,
                    webUrl = webUrl,
                    reference = reference,
                    issue = issue,
                    description = description,
                    state = state,
                    dueDate = dueDate,
                    confidential = confidential,
                    milestone = milestone,
                    milestoneWebPath = milestoneWebPath,
                    milestoneState = milestoneState,
                    milestoneStartDate = milestoneStartDate,
                    milestoneDueDate = milestoneDueDate,
                    labels = labels,
                    timeEstimation = timeEstimation,
                    timeEstimationSeconds = timeEstimationSeconds,
                    totalTimeSpent = totalTimeSpent,
                    totalTimeSpentSeconds = totalTimeSpentSeconds,
                    assignees = assignees,
                    userName = userName,
                    userEmail = userEmail,
                    timeSpent = timeSpent,
                    timeSpentSeconds = timeSpentSeconds,
                    timeSpentAt = timeSpentAt,
                    note = note
                    )
                push!(ans, irow)
                irow = missing
            end
        end
        return ans
    end
end

