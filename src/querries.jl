subQuerryIssues = """
issues {
    nodes {
        id
        iid
        webUrl
        reference
        title
        description
        state
        dueDate
        confidential
        milestone {
            webPath
            title
            state
            startDate
            dueDate
        }
        labels {
            edges {
                node {
                    title
                }
            }
        }
        author {
            name
            username
            publicEmail
        }
        assignees {
            edges {
                node {
                    name
                    username
                    publicEmail
                }
            }
        }
        timeEstimate
        totalTimeSpent
        timelogs {
            edges {
                node {
                    spentAt
                    timeSpent
                    user {
                        name
                        username
                        publicEmail
                    }
                    note {
                        body
                    }
                }
            }
        }
    }
}
"""


querries = Dict{String, Any}(
"currentUser" => """
{
    currentUser {
        name
        username
        publicEmail
    }
}
""",

"groups" => """
{
    currentUser {
        groupMemberships {
            nodes {
                group {
                    id
                    fullPath
                }
            }
        }
    }
}
""",

"issuesGroups" => """
{
    currentUser{
        name
        username
        groupMemberships { 
            nodes {
                group { 
                    fullPath
                    projects {
                        nodes {
                            name
                            fullPath
                            $(subQuerryIssues)
                        }
                    }
                }
            }
        }
    }
}
""",

"issuesProjects" => """
{
    currentUser{
        name
        username
        projectMemberships {
            nodes {
                project {
                    name
                    fullPath
                    group {
                        name
                        fullName
                        fullPath
                    }
                    $(subQuerryIssues)
                }
            }
        }
    }
}
""")

